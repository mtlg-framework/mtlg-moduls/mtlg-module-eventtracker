
function drawMainMenu(){

  // log in demo player if not yet present
  if (!MTLG.getPlayerNumber()) {
      console.log("Logging in");
      MTLG.lc.goToLogin(); //Leave Main Menu and go to login
  }

  // get stage to add objects
  var stage = MTLG.getStageContainer();

  let levelButton = MTLG.utils.uiElements.addButton({
      text: MTLG.lang.getString("level1"),
      sizeX: MTLG.getOptions().width * 0.1,
      sizeY: 70,
    },
    function() {
      MTLG.lc.levelFinished({
        nextLevel: "level1"
      });
    }
  );
  levelButton.x = MTLG.getOptions().width / 2;
  levelButton.y = MTLG.getOptions().height / 2;

  // add objects to stage
  stage.addChild(levelButton);
}
