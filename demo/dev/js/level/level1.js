

function firstlevel_init(){

  drawLevel1();
}

// check wether level 1 is choosen or not
function checkLevel1(gameState){
  if(gameState && gameState.nextLevel && gameState.nextLevel == "level1"){
    return 1;
  }
  return 0;
}


function drawLevel1(){
  var stage = MTLG.getStageContainer();

  //Example "Game"
  var circle1 = new createjs.Shape();
  circle1.graphics.beginFill("red").drawRect(-50, -50, 100, 100);
  circle1.x = 300;
  circle1.y = 500;
  circle1.setBounds(-50, -50, 100, 100)
  circle1.name = "red"
  stage.addChild(circle1)

  circle1.on("pressmove", function(evt) {
    console.log("pressmove", evt);
    evt.target.set(stage.globalToLocal(evt.stageX, evt.stageY));
  });


  var circle = new createjs.Shape();
  circle.graphics.beginFill("blue").drawRect(-50, -50, 100, 100);
  circle.setBounds(-50, -50, 100, 100)
  circle.x = 600;
  circle.y = 500;
  circle.name = "blue"
  stage.addChild(circle)
  circle.on("pressmove", function(evt) {
    evt.target.set(stage.globalToLocal(evt.stageX, evt.stageY));
  });

  //Example use of the Event Tracking Module
  function drawInteraction(interaction){
    var myLine = new createjs.Shape();
    var color = interaction.target.name || "#808080"
    var start_coord = interaction.path[0]

    if(interaction.path.length>2){
      myLine.graphics.setStrokeStyle(4);
      myLine.graphics.beginStroke(color);
      myLine.graphics.moveTo(start_coord.x, start_coord.y);

      interaction.path.forEach(i => {
        myLine.graphics.lineTo(i.x, i.y)

      })
      myLine.graphics.endStroke();
    }else{
      var radius = 10
      myLine.graphics.setStrokeStyle(1)
      myLine.graphics.beginStroke("#404040");
      myLine.graphics.beginFill(color).drawCircle(start_coord.x-radius/2, start_coord.y-radius/2, radius);
    }

    stage.addChild(myLine);
    createjs.Tween.get(myLine).to({alpha: 0},1500).call(() => stage.removeChild(myLine));

  }

  MTLG.eventtracker.addCallback(drawInteraction)
  
}
