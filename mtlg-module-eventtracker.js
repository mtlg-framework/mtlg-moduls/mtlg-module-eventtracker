let init = function( options, callback ){
    stage =  MTLG.getStage();
    stage.enableDOMEvents(true);

    //Down
    stage.addEventListener("mousedown", handleDown);
    stage.addEventListener("stagemousedown", handleDown);

    //Move
    stage.addEventListener("pressmove", handleMove);
    stage.addEventListener("stagemousemove", evt => {
        if(evt.pointerID !== -1) handleMove(evt)
    });

    //Up
    stage.addEventListener("click",handleUp);
    stage.addEventListener("mouseup", handleUp);
    stage.addEventListener("stagemouseup",handleUp);

};

//settings
const check_collisions = true // TODO: give option to set false
let stage_objects = [] //Collision Detection for objects laying under the one interacted with

let stage
let currentInteractions = []; //Stores all ongoing touches & mouse interactions
let callbacks = new Set() //Saves all Callbacks from the Game
let high_callbacks = new Set()
let low_callbacks = new Set()

function generateID(touchid)  {
    return 'id' + (new Date()).getTime() + '#' + touchid
}

function isHighLevel(evt){
    return (evt.target.constructor.name !== "Stage")
}

function isNewInteraction(evt){
    return (currentInteractions[evt.pointerID] === undefined)
}

function createInteraction(evt){
    currentInteractions[evt.pointerID] = {
        id: generateID(evt.pointerID),
        target: evt.target,
        high_level: isHighLevel(evt),
        type: evt.nativeEvent.pointerType,
        path: []
    }
}

function saveCoords(evt){
    var coord = MTLG.getStageContainer().globalToLocal(evt.stageX, evt.stageY);
    currentInteractions[evt.pointerID].path.push({
        underTarget: detectCollisions(coord),
        x: coord.x,
        y: coord.y,
        time: evt.timeStamp
    })
}

function handleDown(evt){
    //Only create new interaction if
    // - not logged already
    // - high level resp. target is not stage
    if (isNewInteraction(evt) || isHighLevel(evt)){
        createInteraction(evt)
        saveCoords(evt)
    }
}

function handleMove(evt){
    //Can only happen for mouse move on stage without pressing down
    if (isNewInteraction(evt)){
        createInteraction(evt)
    }

    //Event is high_level
    if(isHighLevel(evt)){
        saveCoords(evt)
    //Event is low level
    }else{
        //No corresponding high level event
        if(!(currentInteractions[evt.pointerID].high_level)){
            saveCoords(evt)
        }
    }
}

function handleUp(evt){
    handleMove(evt)

    console.log("Interaction over", currentInteractions[evt.pointerID])
    callbacks.forEach(cb => cb(currentInteractions[evt.pointerID]))
    if(currentInteractions[evt.pointerID].high_level){
        high_callbacks.forEach(cb => cb(currentInteractions[evt.pointerID]))
    }else{
        low_callbacks.forEach(cb => cb(currentInteractions[evt.pointerID]))
    }


    delete currentInteractions[evt.pointerID]
}

//Collision Detection
function updateStageObjects(obj){
    if((obj === undefined) || (obj == stage)){
        obj = stage
        stage_objects = []
    }

    if (!(obj.children === undefined)){
        obj.children.forEach(child => {
            stage_objects.push(child)
            updateStageObjects(child)
        })
    }
}

/**
 * detectCollisions - This is a helper function to detect if a
 * event collides with a gameObject
 *
 * @param  {object} coord  Object like {x: 0, y: 0}
 * @return {array} If the coordinates are on a gameObject, it will be returned in an array otherwise an empty array.
 */

function detectCollisions(coord) {
    if(check_collisions) {
        updateStageObjects()
        var objects = stage_objects


        var ret = [];
        objects.forEach(obj => {
            if (obj.parent === null) {
                obj.delete();
            } else {
                let collision = detectCollisionWithGameObject(obj, coord);

                if (collision != null) {
                    ret.push(obj);
                }
            }
        });
        return ret;
    }
}

/**
 * detectCollisionWithGameObject - This function detects a collsion of the
 * coordinate coord with the given gameObject. The coordinate is handles as
 * a square with the size of defaults.gazePrecision. In order to detect a
 * collision the gameObject must have bounds set.
 *
 * @param  {type} gameObject description
 * @param  {type} coord      description
 * @return {type}            description
 */
function detectCollisionWithGameObject(gameObject, coord) {
    let localToGlobalCoord = gameObject.localToGlobal(0,0)
    localToGlobalCoord.x = localToGlobalCoord.x / MTLG.getStageContainer().scaleX;
    localToGlobalCoord.y = localToGlobalCoord.y / MTLG.getStageContainer().scaleY;

    let bounds = gameObject.getBounds(); //MTLG.utils.collision.getBounds(gameObject);

    if(!bounds || (!bounds.x && bounds.x !== 0) || (!bounds.y && bounds.y !== 0)) {
        return null;
    }
    bounds.x = bounds.x + localToGlobalCoord.x;
    bounds.y = bounds.y + localToGlobalCoord.y;

    return MTLG.utils.collision.calculateIntersection(bounds, {x: coord.x , y: coord.y , width: 1, height: 1});
}







//Interface
function addCallback(callback){
    callbacks.add(callback)
}

function removeCallback(callback){
    callbacks.delete(callback)
}

function addHighLevelCallback(callback){
    high_callbacks.add(callback)
}

function removeHighLevelCallback(callback){
    high_callbacks.delete(callback)
}

function addLowLevelCallback(callback){
    low_callbacks.add(callback)
}

function removeLowLevelCallback(callback){
    low_callbacks.delete(callback)
}

// injecting into MTLG
MTLG.eventtracker = {
    addCallback,
    addHighLevelCallback,
    removeCallback,
    removeHighLevelCallback,
    addLowLevelCallback,
    removeLowLevelCallback
};
MTLG.addModule(init);
