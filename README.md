Eine Demo zur beispielhaften Verwendung des Moduls ist im Ordner `Demo` zu finden. Zum Starten muss in diesem Ordner mit der bash `npm install` und anschließend `mtlg serve` ausgeführt werden.

Schnittstellen:
- `addCallback(callback)` Übergibt Callback, das für alle abgeschlossenen Interaktionen aufgerufen wird und das Interactions-Object mit dem Target auf der höchsten Abstraktionsebene überreicht bekommt. Also das Objekt mit dem interagiert wurde, sonst die Stage.
- ` addHighLevelCallback(callback)` Übergibt Callback, das für alle abgeschlossenen Interaktionen mit einem Spielobjekt aufgerufen wird und das Interactions-Object mit dem entsprechendem Spielobjekt als Target überreicht bekommt. Spieleobjekt sollten eigens definierte Attribute haben um diese zu identifizieren.
-  `addLowLevelCallback(callback)` Übergibt Callback, das für alle abgeschlossenen Interaktionen aufgerufen wird und das Interactions-Object mit der Stage überreicht bekommt. Kann generisch verwendet werden.
- `removeCallback(callback)`,`remove...` Entfernt vorher gesetzte Callbacks.

An Callback übergebenes Interaction-Object

| Attribute | Erklärung |  
| --- | --- | 
| **id** | unique ID | 
| **target** | target object of the interaction | 
| **path** | array of points with:| 
| - **time** | timeStamp des Punktes |
| -  **x**| x-Koordinate|
| -  **y** | y-Koordinate |
| -  **underTarget** | darunterliegende Objekte zum Zeitpunkt des Touches |
| **type** | "mouse","touch","pen", ... | 
| **high_level** | true for interactions with gameobject | 




> (outdated) 
> Schnittstellen:
> -  `startListeners(eventtypes) ` Startet Listeners für alle übergebenen Eventtypes in der Form `['click','pressmove','mousedown']`
> -  `stopListeners(eventtypes)` Stoppt  Listeners für alle übergebenen Eventtypes in der Form `['pressmove','mousedown']`
> -  `addCallback(callback,eventtypes)` Übergibt Callback, das für alle definierten Eventtypen aufgerufen wird und das jeweilige Event auf der höchsten Abstraktionsebene überreicht bekommt. Also das High-Level Event falls auf einem Objekt der `Stage`, sonst das Low-Level Event auf dem `Window`
> -  `addLowLevelCallback(callback,eventtypes)` Übergibt Callback, das für alle definierten Eventtypen aufgerufen wird und das jeweilige Low-Level Event  auf dem `Window` übergeben bekommt. Low-Level Events werden bei jeder Interaktion (auf dem Window) aufgerufen.
> - ` addHighLevelCallback(callback,eventtypes)` Übergibt Callback, das für alle definierten Eventtypen aufgerufen wird und das jeweilige High-Level Event auf der `Stage` übergeben bekommt. High-Level Events werden nur aufgerufen, wenn ein Objekt auf der Stage getroffen wird.
> 
> Default-Einstellungen:
> - Es werden automatisch Listener für die Events `['click','pressmove','mousedown']` gestartet, diese können mit startListeners erweitert werden oder mit stopListeners individuell gestoppt werden. Callbacks werden nur für die definierten Eventtypes aufgerufen, wenn ein entsprechender Listener läuft. Das heißt es können beispielsweise einmalig am Anfang Callbacks definiert werden und dynamisch im Spiel die benötigten Listener gestartet und gestoppt werden.
> 
> Übersicht interessanter Attribute der an die Callbacks übergebenen Events:
> 
> | **Low-Level** | Attribute | | **High-Level** | Attribute |
> | --- | --- | --- | --- | --- |
> | Koordinaten | X/Y| | Koordinaten | rawX/Y|
> | | clientX/Y| | | stageX/Y |
> | | pageX/Y| | | |
> | Eventtype |  type | | Eventtype |  type |
> | Time |  timestamp | | Time | nativeEvent.timestamp (gleiche Clock) | _//gleiche Clock wie Low-Level Events_
> | |  | | | timestamp (andere Clock) | _//andere Clock als Low-Level Events, nicht vergleichbar!_
> | | | | Zielobjekt | currentTarget |
> | | | | Low-Level | nativeEvent |
> 
> Ein High-Level Event beinhaltet im Attribut nativeEvent immer das zugehörige Low-Level Event. Da die Stage und das Window andere Clocks verwenden, sind die timestamps der Low- und High-Level Events nicht direkt vergleichbar. Für High-Level Events wird empfohlen über nativeEvent.timestamp die zugehörige timestamp des Windows zuverwenden.
> 
> Auch kann über das currentTarget eines High-Level Events auf die Parents des Objektes, wie beispielsweise Container, zugegriffen  beziehungsweise diese identifizert werden
